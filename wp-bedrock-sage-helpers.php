<?php
/*
Plugin Name:  Sage TidyUp
Plugin URI:   https://samjonsmith.com
Description:  Quick and dirty alternative of soil
Version:      1.0.0
Author:       Sam Jon Smith
Author URI:   https://samjonsmith.com
License:      MIT License
*/

new \SageTidyUp\Theme\TidyUp();